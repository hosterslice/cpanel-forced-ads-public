<?php
class CPFA_WHM extends CPFA_Websites {
	function __call($func, $args) {
		return call_user_func_array(array($this,$func), $args);
	}
	function __construct() {
		@$this->checkacl();
	}
	function die1() {
		header('Location: /cgi/sss');
		exit();
	}
	function checkacl($acl = 'all') {
		if (!WHM) {
			$this->die1();
		}
		$user = $_ENV['REMOTE_USER'];	
		if ($user == 'root') {	
			return null;	
		}
		$reseller = file_get_contents('/var/cpanel/resellers');
		foreach (split('', $reseller) as $line) {
			if (preg_match('/^' . $user . ':/', $line)) {		
				$line = preg_replace('/^' . $user . ':/', '', $line);
				foreach (split(',', $line) as $perm) {			
					if (($perm == 'all' || $perm == $acl)) {			
						return null;			
					}
				}
			}
		}
		$this->die1();
	}
}