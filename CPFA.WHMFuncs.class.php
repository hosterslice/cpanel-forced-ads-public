<?php
class CPFA_WHM_FUNCS extends CPFA_MYSQL {	
	protected $l = null;	
	function __construct() {	
		$this->l = new CPFA_WHM();
	}
	function menu() {	
		$prefix = preg_replace('/(.*?)-(.*)/is', '$1', $this->getKey());
		if ($this->l->ForcedAdsCheck()) {		
			$i = '              <li><a href="' . ADDONFILE . '?p=editAd">Edit Default Ad</a></li>';		
			$i .= '              <li><a href="' . ADDONFILE . '?p=listU">Edit a Users Ad</a></li>';	
			$i .= '              <li><a href="' . ADDONFILE . '?p=listP">Edit a Packages Ad</a></li>';
			return $i;	
		} else {
			return '              <li><a href="' . ADDONFILE . '?p=editAd">Edit Default Ad</a></li>';	
		}
	}	
	function call($func) {		
		$prefix = preg_replace('/(.*?)-(.*)/is', '$1', $this->getKey());
		if (method_exists('CPFA_WHM_FUNCS', $func)) {		
			if (($this->$func(1) == 'PRO' && $prefix == 'BASIC')) {	
				return $this->home();			
			} else {			
				return $this->$func();		
			}		
		} else {		
			return $this->home();	
		}
	}
	function del($pre = 0) {		
		if ($pre == 1) {		
			return 'PRO';		
		}	
		$this->l->removeAd($_REQUEST['user'], 'CPFAForcedAdsScript');		
		return $this->listU();		
	}	
	function up($pre = 0) {		
		if ($pre == 1) {	
			return 'PRO';		
		}	
		$this->l->useP($_GET['user']);
		return $this->listU();
	}
	function np($pre = 0) {
		if ($pre == 1) {
			return 'PRO';
		}
		$this->l->nouseP($_GET['user']);
		return $this->listU();
	}
	function listU($pre = 0) {
		if ($pre == 1) {
			return 'PRO';
		}
		$i .= '<h1>Select a user whose ad you wish to edit:</h1>';
		$i .= '<table class="table">';
		$i .= '  <tbody>';
		foreach (glob('/var/cpanel/users/*') as $user) {
			$user = preg_replace('/(.*)\/(.*)/is', '$2', $user);
			$i .= '<tr class=\'info\'>';
			$i .= '<td width=\'25%\'>';
			$i .= $user . '';
			$i .= '</td>';
			$i .= '<td width=\'50%\'>';
			if ($this->l->hasOwnAd($user)) {
				$i .= 'Has Custom ad. Click <a href=\'' . ADDONFILE . '?p=del&user=' . $user . '\'>here</a> to delete it</a> and <a href=\'' . ADDONFILE . '?p=editUserAd&user=' . $user . '\'>here to edit it</a>';
			} else {
				$i .= 'No Custom Ad. Click <a href=\'' . ADDONFILE . '?p=editUserAd&user=' . $user . '\'>Here</a> to set one.';
			}
			$i .= '</td>';
			$i .= '<td>';
			if ($this->l->usesPAd($user)) {
				$i .= 'Uses Package Ad! Click <a href=\'' . ADDONFILE . '?p=np&user=' . $user . '\'>Here</a> to disable';
			} else {
				$i .= 'Not Using Package Ad! Click <a href=\'' . ADDONFILE . '?p=up&user=' . $user . '\'>Here</a> to enable';
			}
			$i .= '</tr>';
		}
		$i .= '</tbody>';
		$i .= '</table>';
		return $i;
	}
	function listP($pre = 0) {
		if ($pre == 1) {
			return 'PRO';
		}
		$i .= '<h1>Select a package whose ad you wish to edit:</h1>';
		$i .= '<table class="table">';
		$i .= '  <tbody>';
		$pop   = glob('/var/cpanel/packages/*');
		$pop[] = '/var/cpanel/packages/default';
		foreach ($pop as $user) {
			$user = preg_replace('/(.*)\/(.*)/is', '$2', $user);
			$i .= '<tr class=\'info\'>';
			$i .= '<td width=\'25%\'>';
			$i .= $user;
			$i .= '</td>';
			$i .= '<td width=\'50%\'>';
			$i .= '<a href=\'' . ADDONFILE . '?p=editP&pack=' . $user . '\'>Edit Ad</a></td>';
			$i .= '</tr>';
		}
		$i .= '</tbody>';
		$i .= '</table>';
		return $i;
	}
	function editUserAd($pre = 0) {
		if ($pre == 1) {
			return 'PRO';
		}
		if (isset($_REQUEST['edit'])) {
			$this->l->setAd($_REQUEST['ad'], $_REQUEST['user'], 'CPFAForcedAdsScript');
		}
		$i .= '<div class=\'well well-large\'><form action=\'' . ADDONFILE . '\' method=\'post\'>';
		$i .= '<h1>Here you can edit the ad for user ' . $_REQUEST['user'] . '</h1>';
		$i .= '<input type="hidden" name="p" value="editUserAd">' . '';
		$i .= '<input type="hidden" name="user" value="' . $_REQUEST['user'] . '">' . '';
		$i .= '<textarea name="ad" style="width:100%;height:100px">' . $this->l->getAd($_REQUEST['user']) . '</textarea>' . '';
		$i .= '<input type="submit" name="edit" value="Edit Ad">' . '';
		$i .= '</form></div>';
		return $i;
	}
	function editP($pre = 0) {
		if ($pre == 1) {
			return 'PRO';
		}
		if (isset($_REQUEST['edit'])) {
			$this->l->setPAd($_REQUEST['ad'], $_REQUEST['user']);
		}
		$i .= '<div class=\'well well-large\'><h1>Here you can edit the ad for package ' . $_REQUEST['pack'] . '</h1>';
		$i .= '<form method="post" action="' . ADDONFILE . '">' . '';
		$i .= '<input type="hidden" name="user" value="' . $_REQUEST['pack'] . '">' . '';
		$i .= '<input type="hidden" name="p" value="editP">' . '';
		$i .= '<textarea name="ad" style="width:100%;height:100px">';
		$i .= $this->l->getP($_REQUEST['pack']);
		$i .= '</textarea>';
		$i .= '<br />';
		$i .= '<input type="submit" name="edit" value="Edit Ad">' . '';
		$i .= '</form></div>';
		return $i;
	}
	function editAd($pre = 0) {
		if ($pre == 1) {
			return 'ALL';
		}
		if (isset($_REQUEST['edit'])) {
			$this->l->setAd($_REQUEST['ad'], 'cpfa_default_ad', 'CPFAForcedAdsScript');
		}
		$i .= '<div class=\'well well-large\'><h1>Here you can edit the default ad</h1>';
		$i .= '<form method="post" action="' . ADDONFILE . '">' . '';
		$i .= '<input type="hidden" name="p" value="editAd">' . '';
		$i .= '<textarea name="ad" style="width:100%;height:100px">';
		$i .= $this->l->getAd('cpfa_default_ad');
		$i .= '</textarea>';
		$i .= '<br />';
		$i .= '<input type="submit" name="edit" value="Edit Ad">' . '';
		$i .= '</form></div>';
		return $i;
	}
	function home($pre = 0) {
		if ($pre == 1) {
			return 'ALL';
		}
		if (isset($_REQUEST['key'])) {
			$this->l->setKey($_REQUEST['key'], MYSQLPASS);
			$this->l->ForcedAdsCheck(1);
		}
		$lCheck = $this->l->ForcedAdsCheck();
		if (isset($_REQUEST['recheck'])) {
			$lCheck = $this->l->ForcedAdsCheck(1);
		}
		$class = ($lCheck ? 'success' : 'warning');
		$txt   = ($lCheck ? 'Pro License' : 'Basic License');
		$i .= '<table class="table">' . '';
		$i .= '<tbody>';
		$i .= '<tr class="' . $class . '">' . '';
		$i .= '<form action=\'' . ADDONFILE . '\' method=\'post\'><td width=\'25%\'>License Status:</td><td width=\'50%\'>' . $txt . '</td><td><a href=\'' . ADDONFILE . '?recheck=true\'>Recheck</a></td></tr>';
		$i .= '<tr class=\'' . $class . '\'><td width=\'25%\'>Your Key is:</td><td width=\'50%\'><input type=\'text\' maxlen=\'15\' name=\'key\' value=\'' . $this->l->getKey() . '\'></td><td><input type=\'submit\' value=\'Edit Key\'></td></tr></table></form></td>';
		$i .= '</tr>';
		$i .= '</tbody>';
		$i .= '</table>';
		$i .= '<p>';
		$i .= 'Welcome, please choose from the options on the top.';
		$i .= '</p>';
		return $i;
	}
}
