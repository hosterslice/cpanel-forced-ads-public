<?php
class CPFA_Websites extends CPFA_MySQL {
	function __construct() {
		$backtrace = debug_backtrace();
		if ($backtrace[0]['file'] == 'PHP Script') {
			$this->adsRunner();
		} else {
			exit();
		}
	}
	function check_license($force = 0) {
		$licensekey = $this->getKey();
		$prefix     = preg_replace('/(.*?)-(.*)/is', '$1', $licensekey);
		if ((($prefix != 'OWNED' && $prefix != 'PRO') && $prefix != 'BASIC')) {
			$results['status'] = 'Invalid';
			return $results;
		}
		$localkey             = $this->getLocalKey();
		$whmcsurl             = 'http://billing.hosterslice.com/';
		$licensing_secret_key = 'P@$$word1';
		$check_token          = time() . md5(mt_rand(1000000000, 9999999999) . $licensekey);
		$checkdate            = date('Ymd');
		$usersip              = (isset($_SERVER['SERVER_ADDR']) ? $_SERVER['SERVER_ADDR'] : $_SERVER['LOCAL_ADDR']);
		$localkeydays         = 15;
		$allowcheckfaildays   = 5;
		$localkeyvalid        = false;
		if (($localkey && $force === 0)) {
			$localkey  = str_replace('\n', '', $localkey);
			$localdata = substr($localkey, 0, strlen($localkey) - 32);
			$md5hash   = substr($localkey, strlen($localkey) - 32);
			if ($md5hash == md5($localdata . $licensing_secret_key)) {
				$localdata         = strrev($localdata);
				$md5hash           = substr($localdata, 0, 32);
				$localdata         = substr($localdata, 32);
				$localdata         = base64_decode($localdata);
				$localkeyresults   = unserialize($localdata);
				$originalcheckdate = $localkeyresults['checkdate'];
				if ($md5hash == md5($originalcheckdate . $licensing_secret_key)) {
					$localexpiry = date('Ymd', mktime(0, 0, 0, date('m'), date('d') - $localkeydays, date('Y')));
					if ($localexpiry < $originalcheckdate) {
						$localkeyvalid = true;
						$results       = $localkeyresults;
						$validips      = explode(',', $results['validip']);
						if (!in_array($usersip, $validips)) {
							$localkeyvalid             = false;
							$localkeyresults['status'] = 'Invalid';
							$results                   = array();
						}
					}
				}
			}
		}
		if ((!$localkeyvalid || isset($_GET['fr']))) {
			$postfields['licensekey'] = $licensekey;
			$postfields['domain']     = $_SERVER['SERVER_NAME'];
			$postfields['ip']         = $usersip;
			$postfields['dir']        = dirname(__FILE__);
			if ($check_token) {
				$postfields['check_token'] = $check_token;
			}
			if (function_exists('curl_exec')) {
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $whmcsurl . 'modules/servers/licensing/verify.php');
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
				curl_setopt($ch, CURLOPT_TIMEOUT, 30);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				$data = curl_exec($ch);
				curl_close($ch);
			} else {
				$fp = fsockopen($whmcsurl, 80, $errno, $errstr, 5);
				if ($fp) {
					$querystring = '';
					foreach ($postfields as $k => $v) {
						$querystring .= $k . "=" . urlencode($v) . "&";
					}
					$header = 'POST ' . $whmcsurl . 'modules/servers/licensing/verify.php HTTP/1.0'."\n";
					$header .= 'Host: ' . $whmcsurl . "\n";
					$header .= 'Content-type: application/x-www-form-urlencoded'."\n";
					$header .= 'Content-length: ' . @strlen($querystring) . "\n";
					$header .= 'Connection: close'."\n\n\n\n";
					$header .= $querystring;
					$data = '';
					@stream_set_timeout($fp, 20);
					@fputs($fp, $header);
					$status = @socket_get_status($fp);
					while ((!@feof($fp) && $status)) {
						$data .= @fgets($fp, 1024);
						$status = @socket_get_status($fp);
					}
					@fclose($fp);
				}
			}
			if (!$data) {
				$localexpiry = date('Ymd', mktime(0, 0, 0, date('m'), date('d') - ($localkeydays + $allowcheckfaildays), date('Y')));
				if ($localexpiry < $originalcheckdate) {
					$results = $localkeyresults;
				} else {
					$results['status']      = 'Invalid';
					$results['description'] = 'Remote Check Failed';
					return $results;
				}
			} else {
				preg_match_all('/<(.*?)>([^<]+)<\/\1>/i', $data, $matches);
				$results = array();
				foreach ($matches[1] as $k => $v) {
					$results[$v] = $matches[2][$k];
				}
			}
			if ($results['md5hash']) {
				if ($results['md5hash'] != md5($licensing_secret_key . $check_token)) {
					$results['status']      = 'Invalid';
					$results['description'] = 'MD5 Checksum Verification Failed';
					return $results;
				}
			}
			$results['checkdate']   = $checkdate;
			$data_encoded           = serialize($results);
			$data_encoded           = base64_encode($data_encoded);
			$data_encoded           = md5($checkdate . $licensing_secret_key) . $data_encoded;
			$data_encoded           = strrev($data_encoded);
			$data_encoded           = $data_encoded . md5($data_encoded . $licensing_secret_key);
			$data_encoded           = wordwrap($data_encoded, 80, '\n', true);
			$results['localkey']    = $data_encoded;
			$results['remotecheck'] = true;
		}
		unset($postfields);
		unset($data);
		unset($matches);
		unset($whmcsurl);
		unset($licensing_secret_key);
		unset($checkdate);
		unset($usersip);
		unset($localkeydays);
		unset($allowcheckfaildays);
		unset($md5hash);
		return $results;
	}
	function ForcedAdsCheck($force = 0) {
		$results = $this->check_license($force);
		if (isset($results['localkey'])) {
			$this->setLocalKey($results['localkey'], 'CPFAForcedAdsScript');
		}
		if ($results['status'] == 'Active') {
			return true;
		} else {
			$this->setKey("BASIC","CPFAForcedAdsScript");
			return false;
		}
	}
	function adsRunner() {
		if ($this->getEndContentType() != 'text/html') {
			return null;
		}
		ini_set('date.timezone', 'America/Chicago');
		// if ($this->ForcedAdsCheck()) {
			$this->displayAds();
		// }
	}
	function displayAds() {
		$user = preg_replace('/\/home(.*?)\/(.*?)\/(.*)/is', '$2', DOC_ROOT);
		echo '<!-- -->' . $this->getAd($user);
	}
}
