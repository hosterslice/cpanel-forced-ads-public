<?php
class CPFA_MySQL extends CPFA_Functions {
	private $host = 'localhost';
	private $db = 'root_cpfa';
	private $user1 = 'root_cpfa1';
	private $user2 = 'root_cpfa2';
	private $pass1 = 'root_cpfa';
	private $pass2 = '';
	function query($sql, $passPre = '1', $pass = 'default') {
		$this->open($passPre, $pass);
		$data = mysql_query($sql);
		$this->close();
		return $data;
	}
	private function mres($str) {
		return addslashes($str);
	}
	function open($passPre = '1', $pass = 'default') {
		$passVar = 'pass' . $passPre;
		$userVar = 'user' . $passPre;
		$passwd1 = ($pass != 'default') ? $pass : $this->$passVar;
		$passwd  = (!WHM )? $this->$passVar : $passwd1;
		if (!(@mysql_connect($this->host, $this->$userVar, $passwd))) {
			echo mysql_error();
			trigger_error("L",E_USER_ERROR);
		}
		@mysql_select_db($this->db);
	}
	function close() {
		@mysql_close();
	}
	function getKey() {
		$data = $this->query('select `key` from cpfa_config','1');
		$data = @mysql_fetch_array($data);
		return $data['key'];
	}
	function setKey($key, $pass = 'default') {
		$this->query("update cpfa_config set `key`='".$key."'", '2', $pass);
        }
	function getLocalKey() {
		$data = $this->query('select localkey from cpfa_config');
		$data = mysql_fetch_array($data);
		return $data['localkey'];
	}
	function setLocalKey($key, $pass = 'default') {
		$this->query("update cpfa_config set `localkey`='".$key."'", '2', $pass);
	}
	function getDefaultAd() {
		$data = $this->query("select * from `cpfa_ads` where `username`='cpfa_default_ad' limit 1");
		$data = mysql_fetch_array($data);
		return stripslashes($data['ad']);
	}
	function getUserAd($user) {
		$data = $this->query("select * from `cpfa_ads` where `username`='".$user."' limit 1");
		$data = mysql_fetch_array($data);
		return stripslashes($data['ad']);
	}
	function getPackageAd($user) {
		$data1 = $this->query("SELECT * FROM  `cpfa_ads` AS a,  `cpfa_userPacks` AS b WHERE b.user = '".$user."' && b.package = a.username && a.isPack = 1");
		$data  = mysql_fetch_array($data1);
		return stripslashes($data['ad']);
	}
	function getAd($user) {
		$prefix = preg_replace('/(.*?)-(.*)/is', '$1', $this->getKey());
		if (($prefix != 'OWNED' && $prefix != 'PRO') || $user=="cpfa_default_ad") {
			return $this->getDefaultAd();
		}
		if ($this->hasOwnAd($user)) {
			$ad = $this->getUserAd($user);
			if ($ad == "{NONE}") {
				return '';
			} else {
				return $ad;
			}
		}
		
		if ($this->usesPAd($user)) {
			return $this->getPackageAd($user);
		}
		
		return $this->getDefaultAd();
	}
	function removeAd($user, $pass) {
		$prefix = preg_replace('/(.*?)-(.*)/is', '$1', $this->getKey());
		if (($prefix != 'OWNED' && $prefix != 'PRO')) {
			$user = 'cpfa_default_ad';
		}
		$this->query("update cpfa_ads set ad='' where `username`='".$user."' && `isPack`=0 limit 1", '2', $pass);
	}
	function getP($user) {
		$v = '1';
		$prefix = preg_replace('/(.*?)-(.*)/is', '$1', $this->getKey());
		if (($prefix != 'OWNED' && $prefix != 'PRO')) {
			$v = '0';
		}
		$a = mysql_fetch_array($this->query("select `ad` from cpfa_ads where `isPack` = 1 && `username`='".$user."' limit 1"));
		return $a['ad'];
	}
	function hasOwnAd($user) {
		$prefix = preg_replace('/(.*?)-(.*)/is', '$1', $this->getKey());
		if (($prefix != 'OWNED' && $prefix != 'PRO')) {
			$user = 'cpfa_default_ad';
		}
		$a = mysql_num_rows($this->query("select * from cpfa_ads where username='".$user."' && `ad`!='' limit 1"));
		return $a == 1;
	}
	function usesPAd($user) {
		$a = mysql_num_rows($this->query("select * from cpfa_ads where username='".$user."' && `usePackage`=1 limit 1"));
		return $a == 1;
	}
	function setPAd($ad, $p) {
		$prefix = preg_replace('/(.*?)-(.*)/is', '$1', $this->getKey());
		if (($prefix != 'OWNED' && $prefix != 'PRO')) {
			$user = 'cpfa_default_ad';
		}
		$this->query('update `cpfa_ads` set `ad`=\'' . $this->mres($ad) . '\' where `isPack` = 1 && `username`=\'' . $p . '\' limit 1', '2', MYSQLPASS);
	}
	function useP($user) {
		$v = '1';
		$prefix = preg_replace('/(.*?)-(.*)/is', '$1', $this->getKey());
		if (($prefix != 'OWNED' && $prefix != 'PRO')) {
			$v = '0';
		}
		$this->query("update `cpfa_ads` set `usePackage`='".$v."' where `isPack` = 0 && `username`='".$user."' limit 1", '2', MYSQLPASS);
	}
	function nouseP($user) {
		$v = '0';
		$this->query("update `cpfa_ads` set `usePackage`='".$v."' where `isPack` = 0 && `username`='".$user."' limit 1", '2', MYSQLPASS);
		$this->close();
	}
	function setAd($ad, $user = 'cpfa_default_ad', $pass = 'default') {
		$prefix = preg_replace('/(.*?)-(.*)/is', '$1', $this->getKey());
		if (($prefix != 'OWNED' && $prefix != 'PRO')) {
			$user = 'cpfa_default_ad';
		}
		$query = "update `cpfa_ads` set `ad`='".$this->mres($ad)."' where `isPack` = 0 && `username`='".$user."' limit 1";
		if (!($this->query($query, '2', MYSQLPASS))) {
			exit(mysql_error());
		}
	}
}
