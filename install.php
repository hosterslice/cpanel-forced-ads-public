<?php
function buildCMD($cmd, $out=0) {
    if (empty($out))
        $out = 1;
    global $masterCMD;
    $append = ";";
    if ($out == 0) {
        $append = " > /dev/null 2>&1;";
    }
    $masterCMD .= $cmd . $append;
    return;
}
function runCMD($die=0, $run=1) {
    if (empty($die))
        $die = 0;
    if (empty($run))
        $run = 1;
    if ($run != 1) {
        return;
    }
    global $masterCMD;
    echo shell_exec('' . $masterCMD);
    if ($die === 1) {
        exit();
    }
    $masterCMD = "";
    return;
}
function outp($str, $die=0) {
    if (empty($die))
        $die = 0;
    echo $str . "\n";
    if ($die) {
        exit();
    }
    return;
}
function cmd($str, $die, $echo) {
    if (empty($die))
        $die = 0;
    if (empty($echo))
        $echo = 1;
    if ($echo == 0) {
        return;
    }
    echo $str;
    if ($die) {
        exit();
    }
    return;
}
function getInput($msg) {
    fwrite(STDOUT, '' . $msg . ": ");
    $varin = trim(fgets(STDIN));
    return $varin;
}
function err($code) {
    outp("Install Failed: Error Code " . $code . ": " . mysql_error(), 1);
    return;
}
$masterCMD = "";
if (shell_exec("whoami") != "root" . "\n") {
    outp("Must Be Root!", 1);
}
ini_set("html_errors", "off");
cmd(shell_exec("clear"));
outp("Installing: Please Wait");
define("PASS", preg_replace("/(.*)pass=(.*?)\n(.*)/is", "$2", file_get_contents("/root/.my.cnf")));
$db = mysql_connect("localhost", "root", PASS) or err(1);
$restore = "";
mysql_query("create database root_cpfa;", $db) or err(101);
mysql_query("grant SELECT on root_cpfa.* to 'root_cpfa1'@'localhost' identified by 'root_cpfa';", $db) or err(102);
mysql_query("grant ALL PRIVILEGES on root_cpfa.* to 'root_cpfa2'@'localhost' identified by 'CPFAForcedAdsScript';", $db) or err(103);
mysql_select_db("root_cpfa", $db) or err(104);
mysql_query("CREATE TABLE cpfa_config ( `key` TEXT, `localkey` TEXT);", $db) or err(105);
mysql_query("CREATE TABLE cpfa_ads ( `isPack` INT(1) DEFAULT '0', `usePackage` INT(1) DEFAULT '1', ad TEXT, username VARCHAR(50));", $db) or err(106);
mysql_query("create table cpfa_userPacks ( `user` VARCHAR(50),`package` TEXT);", $db);
mysql_query("ALTER TABLE  `cpfa_ads` ADD UNIQUE (`isPack` ,`username`);", $db);
mysql_query("ALTER TABLE  `cpfa_userPacks` ADD UNIQUE (`user`);", $db);
mysql_query("insert into cpfa_ads set ad='<center>Your Ad Here</center>',username='cpfa_default_ad';", $db) or err(107);
mysql_query("insert into cpfa_config set `key`='BASIC',localkey='';", $db) or err(108);
foreach (glob("/var/cpanel/users/*") as $userdata) {
    $username = str_replace("/var/cpanel/users/", "", $userdata);
    $plan     = preg_replace("/(.*?)\nPLAN=(.*?)\n(.*)/is", "\$2", file_get_contents($userdata));
    mysql_query("insert into cpfa_ads set `username`='" . $username . "',`isPack`=0,`ad`='';", $db);
    mysql_query("insert into cpfa_userPacks set `user`='" . $username . "',`package`='" . $plan . "';", $db);
    continue;
}
foreach (glob("/var/cpanel/packages/*") as $plan) {
    $plan = str_replace("/var/cpanel/packages/", "", $plan);
    mysql_query("insert into cpfa_ads set `username`='" . $plan . "',`isPack`=1,`ad`='';", $db);
    continue;
}
mysql_query("insert into cpfa_ads set `username`='default',`isPack`=1,`ad`='';", $db);
buildcmd("rm -rf /phpfa/");
buildcmd("rm -rf /ads/");
buildcmd("mkdir /phpfa/");
runcmd(0);
buildcmd("cd /phpfa/");
buildcmd("wget http://cpanel-forced-ads.com/cpfa.tar");
buildcmd("tar -xvf cpfa.tar");
buildcmd("rm -f cpfa.tar");
buildcmd("mkdir /ads/");
buildcmd("echo '<?php require_once(\"/ads/code.php\");new CPFA_CRON();' > /ads/cron.php");
buildcmd("cd /ads/");
buildcmd("wget http://cpanel-forced-ads.com/code.zip");
buildcmd("unzip code.zip");
buildcmd("rm -f code.zip");
buildcmd("chmod 644 *");
runcmd(0);
$file = "/opt/suphp/etc/suphp.conf";
buildcmd("echo \"application/x-httpd-php=/usr/local/lib/\" >> " . $file);
buildcmd("echo \"application/x-httpd-php4=/usr/local/php4/lib/\" >> " . $file);
buildcmd("echo \"application/x-httpd-php5=/usr/local/lib/\" >> " . $file);
runcmd(0);
buildcmd("cd /phpfa/");
buildcmd("phpize");
buildcmd("./configure");
buildcmd("make install");
runcmd(0);
buildcmd("cd /");
runcmd(0);
buildcmd("rm -rf /phpfa/");
buildcmd("echo 'AddType application/x-httpd-php5 .php5 .php4 .php .php3 .php2 .phtml .htm .html' > /usr/local/apache/conf/includes/post_virtualhost_global.conf");
runcmd(0);
buildcmd("/scripts/rebuildhttpdconf");
buildcmd("echo 'extension=forcedAds.so' >> /usr/local/lib/php.ini");
buildcmd("service httpd restart");
runcmd(0);
buildcmd("echo '* * * * * php -f /ads/cron.php' >> /var/spool/cron/root");
runcmd(0);
buildcmd("echo \"#!/usr/bin/php -q\n<?php\n#WHMADDON:cpfa:cPanel Forced Ads\nnew CPFA_WHM_STUFF();\" > /usr/local/cpanel/whostmgr/docroot/cgi/addon_CPFA.cgi");
buildcmd("chmod +x /usr/local/cpanel/whostmgr/docroot/cgi/addon_CPFA.cgi");
runcmd(0);
buildcmd("rm -f ./install.sh");
buildcmd("rm -f ./install.bin");
runcmd(0);
outp("Congrats, it is now installed!");
outp("Install Succeded!", 1);
return 1;
?>
